@extends('layouts.app')

@section('content')


    <div class="mn-content valign-wrapper">
        <main class="mn-inner container ">

            @include('notification')
            <div class="valign">
                <div class="row">
                    <table class="table">
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Location</th>
                            <th>LGA</th>
                            <th></th>
                        </tr>

                        @foreach($participants as $participant)
                            <tr>
                                <td>{{$participant->code}}</td>
                                <td>{{$participant->name}}</td>
                                <td>{{$participant->phone}}</td>
                                <td>{{$participant->email}}</td>
                                <td>{{$participant->location}}</td>
                                <td>{{$participant->Lga->name}}</td>
                                <td>
                                    <btn onclick="$('.party{{$participant->ptid}}').toggleClass('hide');" class="btn teal">Edit</btn>
                                    <a href="{{url('party/delete/' . $participant->ptid)}}" class="btn red">Delete</a>

                                </td>
                            </tr>
                            <tr>
                            <tr class="party{{$participant->ptid}} hide">

                                <td colspan="7">

                                    <ul class="collapsible" data-collapsible="accordion">
                                        <li>
                                            <div class="collapsible-header"><i class="material-icons">edit</i>Edit Participant {{$participant->code}}</div>
                                            <div class="collapsible-body">
                                                <form method="post" action="{{url('participant/edit/' . $participant->ptid)}}">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                    <label>Code</label>
                                                    <input type="text" class="form-control" name="code" value="{{$participant->code}}" required>

                                                    <label>Name</label>
                                                    <input type="text" class="form-control" name="name" value="{{$participant->name}}" required>

                                                    <label>Phone</label>
                                                    <input type="text" class="form-control" name="phone" value="{{$participant->phone}}" required>

                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="email" value="{{$participant->email}}" required>

                                                    <label>Date Of Birth</label>
                                                    <input type="text" id="dob" class="form-control" name="dob" value="{{$participant->dob}}" required>

                                                    <label>State Of Origin</label>

                                                    <select class="form-control" required name="stateOfOrigin">

                                                        <option>Abuja</option>
                                                        <option>Abia</option>
                                                        <option>Adamawa</option>
                                                        <option>Anambra</option>
                                                        <option>Akwa Ibom</option>
                                                        <option>Bauchi</option>
                                                        <option>Bayelsa</option>
                                                        <option>Benue</option>
                                                        <option>Borno</option>
                                                        <option>Cross River</option>
                                                        <option>Delta</option>
                                                        <option>Ebonyi</option>
                                                        <option>Enugu</option>
                                                        <option>Edo</option>
                                                        <option>Ekiti</option>
                                                        <option>Gombe</option>
                                                        <option>Imo</option>
                                                        <option>Jigawa</option>
                                                        <option>Kaduna</option>
                                                        <option>Kano</option>
                                                        <option>Katsina</option>
                                                        <option>Kebbi</option>
                                                        <option>Kogi</option>
                                                        <option>Kwara</option>
                                                        <option>Lagos</option>
                                                        <option>Nasarawa</option>
                                                        <option>Niger</option>
                                                        <option>Ogun</option>
                                                        <option>Ondo</option>
                                                        <option>Osun</option>
                                                        <option>Oyo</option>
                                                        <option>Plateau</option>
                                                        <option>Rivers</option>
                                                        <option>Sokoto</option>
                                                        <option>Taraba</option>
                                                        <option>Yobe</option>
                                                        <option>Zamfara</option>
                                                    </select>

                                                    <label>Address</label>
                                                    <textarea class="materialize-textarea" name="address" required>{{$participant->address}}</textarea>

                                                    <label>Location</label>
                                                    <input type="text" class="form-control" name="location" value="{{$participant->location}}" required>

                                                    <label>LGA</label>
                                                    <select name="lgid" required>
                                                        @foreach($lgas as $lga)
                                                            <option
                                                                    @if($lga->lgid == $participant->lgid)
                                                                            selected
                                                                    @endif
                                                                    value="{{$lga->lgid}}">{{$lga->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <br><br>

                                                    <button class="btn btn-success">Save</button>
                                                    <a onclick="$('.party{{$participant->ptid}}').addClass('hide');" class="btn red">Cancel</a>

                                                </form>


                                            </div>
                                        </li>
                                    </ul>

                                </td>
                            </tr>

                            </tr>

                        @endforeach
                    </table>
                </div>
            </div>
        </main>
    </div>

    <script>
        $(function () {
            $("#dob").datepicker();
        });
    </script>


@endsection