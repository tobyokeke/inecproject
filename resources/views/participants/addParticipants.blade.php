@extends('layouts.app')

@section('content')


    @include('notification')
    <div class="row">

        <div class="col s12 m6 l6 offset-l3 offset-m3">
            <div class="card white darken-1">
                <div class="card-content z-depth-5 ">
                    <div align="center">
                        <span class="card-title"><img src="assets/images/logo.png" style="height: 100px;"></span>
                        <span class="card-title teal-text">ADD PARTICIPANT</span>
                        {{--<span class="card-title">Sign In</span>--}}

                    </div>
                    <div class="row login">

                        <form method="post" action="{{url('add-participant')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <label>Code</label>
                            <input type="text" class="form-control" name="code" value="{{old('code')}}" required>

                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{old('name')}}" required>

                            <label>Phone</label>
                            <input type="text" class="form-control" name="phone" value="{{old('phone')}}" required>

                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{old('email')}}" required>

                            <label>Date Of Birth</label>
                            <input type="text" id="dob" class="form-control" name="dob" value="{{old('dob')}}" required>

                            <label>State Of Origin</label>

                            <select class="form-control" required name="stateOfOrigin">

                                <option>Abuja</option>
                                <option>Abia</option>
                                <option>Adamawa</option>
                                <option>Anambra</option>
                                <option>Akwa Ibom</option>
                                <option>Bauchi</option>
                                <option>Bayelsa</option>
                                <option>Benue</option>
                                <option>Borno</option>
                                <option>Cross River</option>
                                <option>Delta</option>
                                <option>Ebonyi</option>
                                <option>Enugu</option>
                                <option>Edo</option>
                                <option>Ekiti</option>
                                <option>Gombe</option>
                                <option>Imo</option>
                                <option>Jigawa</option>
                                <option>Kaduna</option>
                                <option>Kano</option>
                                <option>Katsina</option>
                                <option>Kebbi</option>
                                <option>Kogi</option>
                                <option>Kwara</option>
                                <option>Lagos</option>
                                <option>Nasarawa</option>
                                <option>Niger</option>
                                <option>Ogun</option>
                                <option>Ondo</option>
                                <option>Osun</option>
                                <option>Oyo</option>
                                <option>Plateau</option>
                                <option>Rivers</option>
                                <option>Sokoto</option>
                                <option>Taraba</option>
                                <option>Yobe</option>
                                <option>Zamfara</option>
                            </select>

                            <label>Address</label>
                            <textarea class="materialize-textarea" name="address" required>{{old('address')}}</textarea>

                            <label>Location</label>
                            <input type="text" class="form-control" name="location" value="{{old('location')}}" required>

                            <label>LGA</label>
                            <select name="lgid" required>
                                @foreach($lgas as $lga)
                                    <option value="{{$lga->lgid}}">{{$lga->name}}</option>
                                @endforeach
                            </select>
                            <br><br>

                            <button class="btn btn-success">Save</button>
                            <a href="{{url('/home')}}" class="btn btn-danger">Cancel</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        $(function () {
            $("#dob").datepicker();
        });
    </script>


@endsection