<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')


    <div class="mn-content valign-wrapper">
        <main class="mn-inner container ">
            <div class="valign">
                <div class="row">

                    <table class="table striped">
                        <tr>
                            <th>Name</th>
                            <th>Registered Voters</th>
                            <th>Date Created</th>
                        </tr>

                        @foreach($lgas as $lga)
                            <tr>
                                <td>{{$lga->name}}</td>
                                <td>{{$lga->registeredVoters}}</td>
                                <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$lga->created_at)->toDayDateTimeString()}}</td>
                            </tr>

                        @endforeach
                    </table>
                </div>
            </div>
        </main>
    </div>


@endsection