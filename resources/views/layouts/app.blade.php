<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>INEC PORTAL</title>

    <!-- Styles -->
    <link href="{{ asset('css/materialize.css') }}" rel="stylesheet">
    <link href="{{ asset('css/c3.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{url('css/jquery-ui.min.css')}}">

    <script src="{{url('js/jquery.js')}}"></script>
    <script src="{{url('js/materialize.js')}}"></script>

    <script src="{{url('js/d3.min.js')}}"></script>
    <script src="{{url('js/c3.min.js')}}"></script>


    <script type="text/javascript" src="{{url('js/jquery-ui.min.js')}}"></script>

</head>
<body>


<div id="app">

    <ul id="questionsDD" class="dropdown-content">
        <li><a href="{{url('add-question')}}">Add</a></li>
        <li><a href="{{url('view-questions')}}">View</a></li>
    </ul>

    <ul id="participantsDD" class="dropdown-content">
        <li><a href="{{url('add-participant')}}">Add</a></li>
        <li><a href="{{url('view-participants')}}">View</a></li>
    </ul>

    <ul id="lgasDD" class="dropdown-content">
        <li><a href="{{url('add-lga')}}">Add</a></li>
        <li><a href="{{url('view-lgas')}}">View</a></li>
    </ul>

    <ul id="partyDD" class="dropdown-content">
        <li><a href="{{url('add-party')}}">Add</a></li>
        <li><a href="{{url('view-parties')}}">View</a></li>
    </ul>

    <ul id="pollDD" class="dropdown-content">
        <li><a href="{{url('add-poll')}}">Add</a></li>
        <li><a href="{{url('view-polls')}}">View</a></li>
    </ul>

    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">
        <li><a href="{{url('change-password')}}">Change Password</a></li>
        <li class="divider"></li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>

    <nav>
        <div class="nav-wrapper">
            <a href="{{url('/')}}" class="brand-logo">
                <img src="{{url('images/logo.png')}}" style="width: 100px; height: 64px;">
            </a>
            <ul class="right hide-on-med-and-down">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else

                    <li><a href="{{url('home')}}">Home</a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="questionsDD">Questions<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="pollDD">Polls<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="partyDD">Parties<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="participantsDD">Participants<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-button" href="#!"  data-activates="lgasDD">LGAs<i class="material-icons right">arrow_drop_down</i></a></li>

                    <!-- Dropdown Trigger -->
                <li><a class="dropdown-button" href="#!" data-activates="dropdown1">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>

                @endif
            </ul>
        </div>
    </nav>




    @yield('content')
    </div>

    <script>
        $(document).ready(function () {
            $('select').material_select();
        })
    </script>


</body>
</html>
