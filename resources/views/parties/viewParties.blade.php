<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')



    <div class="mn-content valign-wrapper">
        <main class="mn-inner container ">
            @include('notification')
            <div class="valign">
                <div class="row">
                    <table class="table striped">
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Date Created</th>
                            <th></th>
                        </tr>

                        @foreach($parties as $party)
                            <tr>
                                <td>{{$party->name}}</td>
                                <td>{{$party->code}}</td>
                                <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$party->created_at)->toDayDateTimeString()}}</td>
                                <td>
                                    <btn href="{{url('party/edit/' . $party->pid)}}" onclick="$('.party{{$party->pid}}').toggleClass('hide');" class="btn teal">Edit</btn>
                                    <a href="{{url('party/delete/' . $party->pid)}}" class="btn red">Delete</a>
                                </td>
                            </tr>
                            <tr class="party{{$party->pid}} hide">

                                <td colspan="4">

                                    <ul class="collapsible" data-collapsible="accordion">
                                        <li>
                                            <div class="collapsible-header"><i class="material-icons">edit</i>Edit {{$party->name}}</div>
                                            <div class="collapsible-body">
                                                <form method="post" action="{{url('party/edit/' . $party->pid)}}">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                    <label>Name</label>
                                                    <input type="text" class="form-control" name="name" value="{{$party->name}}" required>

                                                    <label>Code</label>
                                                    <input type="text" class="form-control" name="code" value="{{$party->code}}" required>

                                                    <br><br>

                                                    <button class="btn ">Save</button>
                                                    <a onclick="console.log('clicked');$('.party{{$party->pid}}').addClass('hide');" class="btn red">Cancel</a>

                                                </form>


                                            </div>
                                        </li>
                                    </ul>

                               </td>
                            </tr>

                        @endforeach
                    </table>
                </div>
            </div>
        </main>
    </div>


@endsection