@extends('layouts.app')

@section('content')


    @include('notification')
    <div class="row" style="margin-top: 50px;">

        <div class="col s12 m6 l6 offset-l3 offset-m3">
            <div class="card white darken-1">
                <div class="card-content z-depth-5 ">
                    <div align="center">
                        <span class="card-title teal-text">ADD PARTY</span>

                    </div>
                    <div class="row login">

                        <form method="post" action="{{url('add-party')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{old('name')}}" required>

                            <label>Code</label>
                            <input type="text" class="form-control" name="code" value="{{old('code')}}" required>

                            <br><br>

                            <button class="btn btn-success">Save</button>
                            <a href="{{url('/home')}}" class="btn btn-danger">Cancel</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection