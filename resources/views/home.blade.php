<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                        <div class="row">
                            <div class="col s12 m3 l3" style="color:white">
                                <a style="color:white;" href="{{url('view-parties')}}">
                                    <div class="card-panel teal">
                                        <i class="material-icons">home</i>
                                        <span class="card-title">{{count($parties)}}</span>
                                        <span class="header-text">PARTIES</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col s12 m3 l3" style="color:white">
                                <a style="color:white;" href="{{url('view-polls')}}">
                                    <div class="card-panel regent">
                                        <i class="material-icons">email</i>
                                        <span class="card-title">{{count($polls)}}</span>
                                        <span class="header-text">POLLS</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col s12 m3 l3" style="color:white">
                                <a style="color:white;" href="{{url('view-lgas')}}">
                                    <div class="card-panel regent1">
                                        <i class="material-icons">feedback</i>
                                        <span class="card-title">{{count($lgas)}}</span>
                                        <span class="header-text">LGAS</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col s12 m3 l3" style="color:white">
                                <a style="color:white;" href="{{url('view-participants')}}">
                                    <div class="card-panel green">
                                        <i class="material-icons">file_upload</i>
                                        <span class="card-title">{{count($participants)}}</span>
                                        <span class="header-text">PARTICIPANTS</span>
                                    </div>
                                </a>
                            </div>
                        </div>


                        <div class="row">
                            <h5 class="subheader">Recently Added Polls</h5>

                            <table class="table striped">
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Date Created</th>
                                    <th></th>
                                </tr>
                                <?php $count = 0; ?>
                                @foreach($polls as $poll)

                                    @if($count < 3)
                                    <tr>
                                        <td>{{$poll->name}}</td>
                                        <td>{{$poll->desc}}</td>
                                        <td>{{$poll->status}}</td>
                                        <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$poll->created_at)->toDayDateTimeString()}}</td>
                                        <td>
                                            <a href="{{url('poll-details/' . $poll->plid)}}" class="btn">View</a>
                                        </td>
                                    </tr>
                                    @endif
                                    <?php $count++ ?>
                                @endforeach
                            </table>
                        </div>

                        <br><br>

                        <div class="row">
                            <h5 class="subheader">Recently Registered Participants</h5>

                            <table class="table striped">
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                    <th>LGA</th>
                                    <th></th>
                                </tr>

                                <?php $count = 0; ?>
                                @foreach($participants as $participant)

                                    @if($count < 3)
                                    <tr>
                                        <td>{{$participant->code}}</td>
                                        <td>{{$participant->name}}</td>
                                        <td>{{$participant->phone}}</td>
                                        <td>{{$participant->email}}</td>
                                        <td>{{$participant->location}}</td>
                                        <td>{{$participant->Lga->name}}</td>
                                        <td>
                                            <btn onclick="$('.party{{$participant->ptid}}').toggleClass('hide');" class="btn teal">Edit</btn>
                                            <a href="{{url('party/delete/' . $participant->ptid)}}" class="btn red">Delete</a>

                                        </td>
                                    </tr>
                                    @endif
                                    <?php $count++; ?>
                                @endforeach
                            </table>

                        </div>

                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
