<?php use Illuminate\Support\Facades\Input; ?>
@extends('layouts.app')

@section('content')


    @include('notification')
    <div class="row" style="margin-top: 50px;">

        <div class="col s12 m6 l6 offset-l3 offset-m3">
            <div class="card white darken-1">
                <div class="card-content z-depth-5 ">
                    <div align="center">
                        <span class="card-title teal-text">FINISH POLL</span>
                        <span style="text-align: center;color:darkred;">Please answer all questions</span>

                    </div>
                    <div class="row login">

                        <form method="post" action="{{url('finish-poll')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="plid" value="{{$poll->plid}}">

                            @foreach($poll->Questions as  $question)
                                <h5 class="card-title">{{$question->question}}</h5>
                                <label>Answer</label>
                                <textarea name="answer" class="materialize-textarea" required></textarea>
                            @endforeach


                            <button class="btn btn-success">Save</button>
                            <a href="{{url('/poll-details/' . $poll->plid)}}" class="btn btn-danger">Cancel</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection