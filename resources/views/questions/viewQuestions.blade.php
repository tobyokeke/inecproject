<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')



    <div class="mn-content valign-wrapper">
        <main class="mn-inner container ">
            @include('notification')
            <div class="valign">
                <div class="row">
                    <table class="table striped">
                        <tr>
                            <th>Question</th>
                            <th>Answer</th>
                            <th>Poll</th>
                            <th>Date Created</th>
                            <th></th>
                        </tr>

                        @foreach($questions as $question)
                            <tr>
                                <td>{{$question->question}}</td>
                                <td>{{$question->answer}}</td>
                                <td>{{$question->Poll->name}}</td>
                                <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$question->created_at)->toDayDateTimeString()}}</td>
                                <td>
                                    @if(!isset($question->answer))
                                        <btn href="{{url('question/edit/' . $question->qid)}}" onclick="$('.party{{$question->qid}}').toggleClass('hide');" class="btn teal">Edit</btn>
                                        <a href="{{url('question/delete/' . $question->qid)}}" class="btn red">Delete</a>
                                    @endif

                                </td>
                            </tr>
                            <tr class="party{{$question->qid}} hide">

                                <td colspan="4">

                                    <ul class="collapsible" data-collapsible="accordion">
                                        <li>
                                            <div class="collapsible-header"><i class="material-icons">edit</i>Edit {{$question->name}}</div>
                                            <div class="collapsible-body">
                                                <form method="post" action="{{url('question/edit/' . $question->qid)}}">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                    <label>Name</label>
                                                    <input type="text" class="form-control" name="name" value="{{$question->name}}" required>

                                                    <label>Code</label>
                                                    <input type="text" class="form-control" name="code" value="{{$question->code}}" required>

                                                    <br><br>

                                                    <button class="btn ">Save</button>
                                                    <a onclick="console.log('clicked');$('.party{{$question->qid}}').addClass('hide');" class="btn red">Cancel</a>

                                                </form>


                                            </div>
                                        </li>
                                    </ul>

                                </td>
                            </tr>

                        @endforeach
                    </table>
                </div>
            </div>
        </main>
    </div>


@endsection