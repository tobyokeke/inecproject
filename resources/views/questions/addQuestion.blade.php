<?php use Illuminate\Support\Facades\Input; ?>
@extends('layouts.app')

@section('content')


    @include('notification')
    <div class="row" style="margin-top: 50px;">

        <div class="col s12 m6 l6 offset-l3 offset-m3">
            <div class="card white darken-1">
                <div class="card-content z-depth-5 ">
                    <div align="center">
                        <span class="card-title teal-text">ADD QUESTION</span>

                    </div>
                    <div class="row login">

                        <form method="post" action="{{url('add-question')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <div>
                                @if(Input::has('p'))
                                    <input type="hidden" name="plid" value="{{Input::get('p')}}">
                                @else
                                    <select required name="plid">
                                    @foreach($polls as $poll)
                                        <option value="{{$poll->plid}}">{{$poll->name}}</option>
                                    @endforeach
                                    </select>
                                    <label class="select-label">&nbsp;</label>
                                @endif
                            </div>

                            <label>Question</label>
                            <input type="text" class="form-control" name="question" required>


                            <button class="btn btn-success">Save</button>
                            <a href="{{url('/home')}}" class="btn btn-danger">Cancel</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection