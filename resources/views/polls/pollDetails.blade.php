<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    @include('notification')
    <div class="mn-content valign-wrapper">
        <main class="mn-inner container ">
            <div class="valign">
                <div class="row">

                    <div class="col m12 l12 s12">
                        <div id="chart" style="width:100%"></div>
                    </div>

                    <div class="col m12 l12 s12 " style="padding:50px;">

                        @if($poll->status == "Pending")
                            <a href="{{url('add-question?p=' . $poll->plid)}}" class="btn teal">Add Question</a>
                            <a href="{{url('view-questions')}}" class="btn teal">View Questions</a>
                            <a href="{{url('finish-poll/' . $poll->plid)}}" class="btn teal">Finish Poll</a>
                            <a href="{{url('export/' . $poll->plid)}}" class="btn green">Export</a>
                        @endif

                        @if($poll->status == "Complete")
                                <a href="{{url('view-questions?p=' . $poll->plid)}}" class="btn teal">View Questions</a>
                        @endif
                        <br>

                        <h3 class="card-title">Poll ID - {{$poll->plid}}</h3>

                        Poll - {{$poll->name}} <br>
                        Description - {{$poll->desc}} <br>

                        <div class="stat-container"></div>

                    </div>


                </div>
            </div>
        </main>
    </div>


    <script>

        function getResults() {
            $.ajax({
                url:"{{url('api/get-results/'. $poll->plid)}}",
                method: "get",
                success: function (response) {
                    var data = response;

                    for(var i = 0; i < data.length; i++){

                        $('#party' + i ).text(data[i].party);
                        $('#partyVotes' + i ).text(data[i].votes);

                    }

                    console.log('ran');
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        function first(){
            $.ajax({
                url:"{{url('api/get-results/'. $poll->plid)}}",
                method: "get",
                success: function (response) {
                    var data = response;

                    for(var i = 0; i < data.length; i++){

                        console.log(data[i]);
                        $('.stat-container').append(

                            '<div id="party'+ i + '" class="col m4">' + '</div>' +
                            '<p id="partyVotes' + i + '"></p>');

                        $('#party' + i ).text(data[i].party);
                        $('#partyVotes' + i ).text(data[i].votes);
                    }


                    console.log('ran first');
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }

        function setupChart() {

            $.ajax({
                url: "{{url('api/get-chart-results/'. $poll->plid)}}",
                method: "get",
                success: function (response) {

                    var chart = c3.generate({
                        data: {
                            // iris data from R
                            columns: response,
                            type : 'pie',
                            onclick: function (d, i) { console.log("onclick", d, i); },
                            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                        }
                    });

                    updateChart(chart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }


        function updateChart(chart) {

            setInterval(function () {
                $.ajax({
                    url: "{{url('api/get-chart-results/'. $poll->plid)}}",
                    method: "get",
                    success: function (response) {

                        chart.load({
                            columns: response
                        });

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            },5000);

        }

        $(document).ready(function () {
            first();
            setupChart();
            setInterval(getResults,5000);

        });

    </script>

@endsection