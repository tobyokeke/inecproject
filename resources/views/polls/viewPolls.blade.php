<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')


    <div class="mn-content valign-wrapper">
        <main class="mn-inner container ">
            <div class="valign">
                <div class="row">

                    <table class="table striped">
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th></th>
                        </tr>

                        @foreach($polls as $poll)
                            <tr>
                                <td>{{$poll->name}}</td>
                                <td>{{$poll->desc}}</td>
                                <td>{{$poll->status}}</td>
                                <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$poll->created_at)->toDayDateTimeString()}}</td>
                                <td>
                                    <a href="{{url('poll-details/' . $poll->plid)}}" class="btn">View</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </main>
    </div>


@endsection