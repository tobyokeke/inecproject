<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class participant extends Model
{
    protected $primaryKey = 'ptid';

    protected $guarded = [];

	public function Lga() {
		return $this->hasOne(lga::class,'lgid','lgid');
    }
}
