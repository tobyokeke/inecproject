<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class result extends Model
{
    protected $primaryKey = 'resid';

	public function Party() {
		return $this->belongsTo(party::class,'pid','pid');
    }

	public function Poll() {
		return $this->belongsTo(poll::class,'plid','plid');
    }

	public function Participant() {
		return $this->belongsTo(participant::class,'ptid','ptid');
    }
}
