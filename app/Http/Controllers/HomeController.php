<?php

namespace App\Http\Controllers;

use App\lga;
use App\participant;
use App\party;
use App\poll;
use App\question;
use App\result;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use PHPExcel;
use PHPExcel_IOFactory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$parties = party::all();
    	$polls = poll::all();
    	$lgas = lga::all();
    	$participants = participant::all();

        return view('home',[
        	'parties' => $parties,
	        'polls' => $polls,
	        'lgas' => $lgas,
	        'participants' => $participants
        ]);
    }

	public function addPoll() {
		return view('polls.addPoll');
    }

	public function viewPolls() {
    	$polls = poll::all();
		return view('polls.viewPolls',[
			'polls' => $polls
		]);
    }

	public function pollDetails( $plid ) {
		$poll = poll::find($plid);
		$parties = party::all();

		return view('polls.pollDetails',[
			'poll' => $poll,
			'parties' => $parties
		]);
    }
	public function addParty() {
		return view('parties.addParty');
    }

	public function viewParties() {
		$parties = party::all();

		return view('parties.viewParties',[
			'parties' => $parties
		]);
    }

	public function addQuestion() {
    	$polls  = poll::all();

		return view('questions.addQuestion',[
			'polls' => $polls
		]);
    }

	public function bulkAddLgas(Request $request) {

		/** Create a new Excel5 Reader  **/
//		try {



			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads",$inputFileName);

//			$downloadUrl = url("/uploads/".$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/".$inputFileName);

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);



			// add results to data base from file
			foreach($rowData as $cell) {

//				try{

					if ( ! empty( $cell[0] ) ) {

						$lga = new lga();
						$lga->uid = Auth::user()->uid;
						$lga->name = $cell[0];
						$lga->registeredVoters = $cell[3];
						$lga->save();

					}
//				}
//				catch(Exception $e){}

			}

			$request->session()->flash('success','Lgas Added.');

//		}
//		catch (Exception $e) {
//
//			$request->session()->flash('error',"Sorry an error occurred.");
//		}

		return redirect('add-lga');
	}

	public function viewQuestions() {
		$questions = question::all();

		if(Input::has('p')) $questions = question::where('plid',Input::get('p'))->get();

    	return view('questions.viewQuestions',[
		    'questions' => $questions
		]);
    }

	public function export( $plid ) {
    	$poll = poll::find($plid);
    	$data = json_decode(file_get_contents(url('api/get-results/' . $plid)));

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("SMS DB")
		            ->setTitle("Election Export - " . $poll->name . " - " . Carbon::now()->toDayDateTimeString() )
		            ->setSubject("PHPExcel Test Document")
		            ->setDescription($poll->desc);

//		$results = result::where('plid',$plid)->get();


		$response = array();

		$poll = poll::find($plid);
		$parties = party::all();



			$resultItem = array();

			foreach($poll->Results as $item){

				foreach($parties as $party) {
					if ( $item->pid == $party->pid ) {

						$resultItem = array(
							[
								'party' => $party->name,
								'lga'   => $item->Participant->Lga->name,
								'votes' => $item->votes
							]
						);
					}

				}

			array_push($response,$resultItem);
		}


		$response = collect($response);


		$cell = 2;

		foreach ($response as $item){

					foreach($item as $prop) {
						try {
							$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'A1', "PARTIES" );
							$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'A' . $cell, $prop['party'] );


							$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'B1', "LGAS" );
							$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'B' . $cell, $prop['lga'] );

							$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'C1', "VOTES" );
							$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'C' . $cell, $prop['votes'] );
						} catch (Exception $exception){}
					}
			$cell++;
		}

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
// Save Excel 2007 file
//		echo date('H:i:s') , " Write to Excel2007 format" , EOL;
		$callStartTime = microtime(true);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save("ElectionExport-" . $poll->name . ".xlsx");
		$callEndTime = microtime(true);
		$callTime = $callEndTime - $callStartTime;
//		echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
//// Echo memory usage
//		echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;

		return  response()->download("ElectionExport-" . $poll->name . ".xlsx");
	}

	public function postAddQuestion( Request $request ) {

    	try{
		    $question = new question();
		    $question->question = $request->input('question');
		    $question->plid = $request->input('plid');
		    $question->save();

		    $request->session()->flash('success','Question Added.');
		    return redirect('/poll-details/' . $question->plid);

	    }   catch(\Exception $exception){

    		$request->session()->flash('error','Sorry an error occurred. Try again');
		    return redirect('/poll-details/' . $question->plid);
	    }
    }

	public function finishPoll($plid) {
		$poll = poll::find($plid);

    	return view('questions.finishPoll',[
    		'poll' => $poll
	    ]);
    }

	public function postFinishPoll( Request $request ) {
		$plid = $request->input('plid');
		$poll = poll::find($plid);
		$poll->status = 'Complete';
		$poll->save();
		$request->session()->flash('success','Poll Completed.');
		return redirect('poll-details/' . $poll->plid);
    }
	public function postEditParty(Request $request, $pid) {
    	$party = party::find($pid);
    	$party->update($request->all());
    	$party->save();

    	$request->session()->flash('success',"Party Edited");
    	return redirect('/view-parties');
	}

	public function addParticipant() {
    	$lgas = lga::all();
		return view('participants.addParticipants',[
			'lgas' => $lgas
		]);
    }

	public function addlga() {
		return view('lgas.addLga');
    }

	public function viewLgas() {
		$lgas = lga::all();
		return view('lgas.viewLgas',[
			'lgas' => $lgas
		]);
    }

	public function changePassword() {
		return view('changePassword');
	}

	public function postChangePassword( Request $request ) {
		$oldpassword = $request->input('oldpassword');
		$password = $request->input('password');
		$confirmpassword = $request->input('confirmpassword');
		$user = User::find(Auth::user()->uid);

		if(password_verify($oldpassword,$user->password) ){

			if($confirmpassword == $password){
				$user->password = bcrypt($password);
				$request->session()->flash("success","Password changed successfully");
				$user->save();
				return redirect('/change-password');

			}
			else{
				$request->session()->flash("error", "Both new passwords don't match. Please try again");
				return redirect('/change-password');
			}

		} else {
			$request->session()->flash("error", "Current Password is wrong. Please try again.");
			return redirect('/change-password');
		}

	}
	public function postAddParty(Request $request) {

    	try{
		    $party = new party();
		    $party->name = $request->input('name');
		    $party->code = $request->input('code');
		    $party->uid = Auth::user()->uid;
		    $party->save();
		    $request->session()->flash('success','Party Added');
		    return redirect('add-party');
	    }catch (\Exception $exception){
		    $request->session()->flash('error', 'Sorry an error occurred');
		    return redirect('add-party');
	    }


    }

	public function postAddLga(Request $request) {
		try{
			$lga = new lga();
			$lga->name = $request->input('name');
			$lga->registeredVoters = $request->input('registeredVoters');
			$lga->uid = Auth::user()->uid;
			$lga->save();
			$request->session()->flash('success', 'LGA Added.');

			return redirect('add-lga');

		}catch (\Exception $exception){
			$request->session()->flash('error', 'Sorry an error occurred');
			return redirect('add-lga');

		}
    }


	public function postAddParticipant( Request $request ) {

    	try{

		    $participant = new participant();
		    $participant->lgid = $request->input('lgid');
		    $participant->uid = Auth::user()->uid;
		    $participant->code = $request->input('code');
		    $participant->name = $request->input('name');
		    $participant->email = $request->input('email');
		    $participant->phone = $request->input('phone');
		    $participant->dob = $request->input('dob');
		    $participant->stateOfOrigin = $request->input('stateOfOrigin');
		    $participant->address = $request->input('address');
		    $participant->location = $request->input('location');
		    $participant->save();

		    $request->session()->flash('success','Participant Added.');

		    return redirect('add-participant');
	    }catch (\Exception $exception){
		    $request->session()->flash('error','An error occurred. Please try again.');
		    return redirect('add-participant');
	    }

    }

	public function postAddPoll( Request $request ) {
		$poll = new poll();
		$poll->name = $request->input('name');
		$poll->desc = $request->input('desc');
		$poll->status = 'Pending';
		$poll->uid = Auth::user()->uid;
		$poll->save();
		$request->session()->flash('success','Poll Added.');

		return redirect('add-poll');


    }



	public function postEditParticipant( Request $request, $ptid ) {
		$participant = participant::find($ptid);
		$participant->update($request->all());
		$participant->save();

		$request->session()->flash('success','Participant Edited.');

		return redirect ('view-participants');
    }

	public function viewParticipants() {
		$participants = participant::all();
		$lgas = lga::all();
		return view('participants.viewParticipants',[
			'participants' => $participants,
			'lgas' => $lgas
		]);
    }
}
