<?php

namespace App\Http\Controllers;

use App\participant;
use App\party;
use App\poll;
use App\result;
use Illuminate\Http\Request;

class ApiController extends Controller
{

	public function addResult( Request $request ) {

		try{
		$webhook_secret = 'DRFAK623XENX9HZ4K9ZZA6PNCP9C4KUH';

		if ($request->input('secret') !== $webhook_secret)
		{
			header('HTTP/1.1 403 Forbidden');
			echo "Invalid webhook secret";
		}
		else
		{
			if ($request->input('event') == 'incoming_message')
			{
				$content = $_POST['content'];
				$from_number = $_POST['from_number'];

				$content = explode(' ',$content);

				$participant = participant::where('code',$content[0])->first();

				$poll = poll::find($content[1]);

				$sequence =  $content[2];

				if(result::where('phone',$from_number)->where('plid',$poll->plid)->count() > 0)
					$previousSequence = result::where('phone',$from_number)->where('plid',$poll->plid)->get()->last()->sequence;
				else
					$previousSequence = 0;

				if($poll->status == "Pending") {
					if ( $sequence > $previousSequence ) {

						$totalVotes = 0;
						for ( $i = 3; $i < count( $content ); $i ++ ) {
							$data       = explode( ':', $content[ $i ] );
							$totalVotes += $data[1];
						}

						if ( $totalVotes > $participant->Lga->registeredVoters ) {
							header( "Content-Type: application/json" );
							echo json_encode( array(
								'messages' => array(
									array( 'content' => "Votes are more than registered voters! Please check your input." )
								)
							) );

							return;
						}

						for ( $i = 3; $i < count( $content ); $i ++ ) {

							$data      = explode( ':', $content[ $i ] );
							$partyCode = $data[0];
							$votes     = $data[1];

							$party = party::where( 'code', $partyCode )->first();


							$result           = new result();
							$result->ptid     = $participant->ptid;
							$result->votes    = $votes;
							$result->sequence = $sequence;
							$result->pid      = $party->pid;
							$result->plid     = $poll->plid;
							$result->phone    = $from_number;
							$result->save();

						}

						// do something with the message, e.g. send an autoreply
						header( "Content-Type: application/json" );
						echo json_encode( array(
							'messages' => array(
								array( 'content' => "Thanks for your message!" )
							)
						) );
					} else {
						header( "Content-Type: application/json" );
						echo json_encode( array(
							'messages' => array(
								array( 'content' => "Error. Duplicate message." )
							)
						) );

					}
				} else {
					header( "Content-Type: application/json" );
					echo json_encode( array(
						'messages' => array(
							array( 'content' => "Sorry poll is not closed or canceled." )
						)
					) );

				}

			}
		}
		}catch(\Exception $exception){
			header("Content-Type: application/json");
			echo json_encode(array(
				'messages' => array(
					array('content' => "An error occurred. Please check the format of your message.")
				)
			));

		}
	}

	public function getResults($plid) {

		$response = array();

		$poll = poll::find($plid);
		$parties = party::all();


		foreach($parties as $party){
			$resultItem = array();
			$resultItem['party'] = $party->name;
			$resultItem['votes'] = 0;

			foreach($poll->Results as $item){

				if($item->pid == $party->pid){
					$resultItem['votes'] += $item->votes;
				}

			}

			array_push($response,$resultItem);
		}

		return $response;

	}

	public function getChartResults($plid) {

		$response = array();

		$poll = poll::find($plid);
		$parties = party::all();

		foreach($parties as $party){
			$resultItem = array();
			$totalVotes = 0;

			foreach($poll->Results as $item){
				$totalVotes+= $item->votes;
			}


			array_push($resultItem,$party->name);
			$partyVotes = 0;

			foreach($poll->Results as $item){

				if($item->pid == $party->pid){
					$partyVotes += $item->votes;
				}
			}
			$piePercent = round($partyVotes/$totalVotes * 360,0);
			array_push($resultItem,$piePercent);

			array_push($response,$resultItem);
		}


		return $response;

	}

}
