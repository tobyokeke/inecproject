<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class poll extends Model
{
    protected $primaryKey = 'plid';

	public function Results() {
		return $this->hasMany(result::class,'plid','plid');
    }

	public function Questions() {
		return $this->hasMany(question::class,'plid','plid');
    }
}
