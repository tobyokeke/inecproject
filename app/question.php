<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    protected $primaryKey = 'qid';

	public function Poll() {
		return $this->belongsTo(poll::class,'plid','plid');
    }
}
