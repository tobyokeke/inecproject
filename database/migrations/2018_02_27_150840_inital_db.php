<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class initalDb extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('parties', function(Blueprint $table){
			$table->increments('pid');
			$table->string('name');
			$table->string('code');
			$table->integer('uid');
			$table->timestamps();
		});

		Schema::create('lgas', function(Blueprint $table){
			$table->increments('lgid');
			$table->integer('uid');
			$table->string('name');
			$table->integer('registeredVoters');
			$table->timestamps();
		});

		Schema::create('polls', function(Blueprint $table){
			$table->increments('plid');
			$table->integer('uid');
			$table->string('name');
			$table->string('desc',2000);
			$table->enum('status',['Pending','Complete','Canceled']);
			$table->timestamps();
		});

		Schema::create('results', function(Blueprint $table){
			$table->increments('resid');
			$table->integer('plid');
			$table->integer('votes');
			$table->integer('ptid');
			$table->integer('pid');
			$table->string('phone');
			$table->integer('sequence');
			$table->timestamps();
		});

		Schema::create('questions', function(Blueprint $table){
			$table->increments('qid');
			$table->integer('plid');
			$table->string('question');
			$table->boolean('answer')->nullable();
			$table->timestamps();
		});

		Schema::create('participants', function(Blueprint $table){

			$table->increments('ptid');
			$table->integer('lgid');
			$table->integer('uid');
			$table->string('name');
			$table->string('email');
			$table->string('phone');
			$table->string('dob');
			$table->string('code');
			$table->string('stateOfOrigin');
			$table->string('address');
			$table->string('location');
			$table->timestamps();

		});

		Schema::create('users', function (Blueprint $table) {
			$table->increments('uid');
			$table->string('name');
			$table->string('phone');
			$table->string('email',191)->unique();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
		});

		Schema::create('password_resets', function (Blueprint $table) {
			$table->string('email',191)->index();
			$table->string('token');
			$table->timestamp('created_at')->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::dropIfExists('parties');
		Schema::dropIfExists('lgas');
		Schema::dropIfExists('polls');
		Schema::dropIfExists('results');
		Schema::dropIfExists('questions');
		Schema::dropIfExists('participants');
		Schema::dropIfExists('users');
		Schema::dropIfExists('password_resets');

	}
}
