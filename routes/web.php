<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/add-participant','HomeController@addParticipant');
Route::get('/view-participants','HomeController@viewParticipants');

Route::get('/add-lga','HomeController@addLga');
Route::get('/view-lgas','HomeController@viewLgas');

Route::get('/add-party','HomeController@addParty');
Route::get('/view-parties','HomeController@viewParties');

Route::get('/change-password','HomeController@changePassword');

Route::get('/add-poll','HomeController@addPoll');
Route::get('/view-polls','HomeController@viewPolls');
Route::get('/poll-details/{plid}','HomeController@pollDetails');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/add-question','HomeController@addQuestion');
Route::get('/view-questions','HomeController@viewQuestions');
Route::get('/finish-poll/{plid}','HomeController@finishPoll');

Route::get('/export/{plid}','HomeController@export');


Route::post('/bulk-add-lga','HomeController@bulkAddLgas');
Route::post('finish-poll','HomeController@postFinishPoll');
Route::post('/add-question','HomeController@postAddQuestion');
Route::post('/change-password','HomeController@postChangePassword');
Route::post('/party/edit/{pid}','HomeController@postEditParty');
Route::post('/participant/edit/{ptid}','HomeController@postEditParticipant');
Route::post('/add-poll','HomeController@postAddPoll');
Route::post('/add-party','HomeController@postAddParty');
Route::post('/add-participant','HomeController@postAddParticipant');
Route::post('/add-lga','HomeController@postAddLga');